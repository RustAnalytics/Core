﻿using System;
using System.Collections.Generic;
using ConVar;
using Newtonsoft.Json;
using Oxide.Core.Plugins;

namespace Oxide.Plugins
{
    [Info("RemoteRust Core", "AnExiledGod", "0.3.0")]
    [Description("")]
    
    class RemoteRustCore : CovalencePlugin
    {
        [PluginReference] private Plugin RemoteRust;
        
        private static PluginConfig _config;
        
        private static long GetUnixTimestamp()
        {
            return DateTimeOffset.UtcNow.ToUnixTimeSeconds();
        }

        #region Initialization

        private void Loaded()
        {
            if (RemoteRust == null)
            {
                Puts("RemoteRust extension is required for this plugin to work correctly.");

                return;
            }

            RemoteRust.Call("SetEndpoint", _config.Endpoint);

            UnityEngine.Application.logMessageReceived += ConsoleLoggingHandler;
        }

        private void Unload()
        {
            UnityEngine.Application.logMessageReceived -= ConsoleLoggingHandler;
        }

        #endregion

        #region Configuration

        private class PluginConfig
        {
            public string AuthenticationKey;
            public string Endpoint;
        }

        private void Init()
        {
            _config = Config.ReadObject<PluginConfig>();
        }

        protected override void LoadDefaultConfig()
        {
            Config.WriteObject(GetDefaultConfig(), true);
        }

        protected override void SaveConfig()
        {
            Config.WriteObject(_config, true);
        }

        private PluginConfig GetDefaultConfig()
        {
            return new PluginConfig
            {
                AuthenticationKey = "",
                Endpoint = "production"
            };
        }

        #endregion

        #region Helper Functions

        [HookMethod("GetVersion")]
        private string GetVersionHook()
        {
            return Version.ToString();
        }

        [HookMethod("LogToFile")]
        private void LogToFileHook(string filename, string text)
        {
            LogToFile(filename, text, null);
        }

        [HookMethod("GetOnlinePlayers")]
        private List<ulong> GetOnlinePlayers()
        {
            List<ulong> players = new List<ulong>();

            foreach (BasePlayer player in BasePlayer.activePlayerList)
            {
                players.Add(player.userID);
            }

            return players;
        }

        [HookMethod("GetSleepingPlayers")]
        private List<ulong> GetSleepingPlayers()
        {
            List<ulong> players = new List<ulong>();

            foreach (BasePlayer player in BasePlayer.sleepingPlayerList)
            {
                if (player.UserIDString.Length != 17) { continue; }

                players.Add(player.userID);
            }

            return players;
        }

        [HookMethod("GetAllAlivePlayers")]
        private List<ulong> GetAllAlivePlayers()
        {
            List<ulong> players = new List<ulong>();

            players.AddRange(GetOnlinePlayers());
            players.AddRange(GetSleepingPlayers());

            return players;
        }

        #endregion

        #region DataClass

        public class CoreData
        {
            public string AuthKey;
            public long Timestamp;

            public CoreData()
            {
                AuthKey = _config.AuthenticationKey;
                Timestamp = GetUnixTimestamp();
            }
        }

        #endregion
        
        #region Console Logging

        private class OnConsoleMessageData : CoreData
        {
            public string Type = "OnConsoleMessage";
            public string LogString;
            public string StackTrace;
            public string LogType;
        }

        private void ConsoleLoggingHandler(string logMessage, string stackTrace, UnityEngine.LogType type)
        {
            OnConsoleMessageData payload = new OnConsoleMessageData
            {
                LogString = logMessage,
                StackTrace = stackTrace,
                LogType = type.ToString()
            };

            RemoteRust.Call("AddMessageToQueue", JsonConvert.SerializeObject(payload));
        }

        #endregion

        #region OnServerInitialized
        
        class OnServerInitializedData : CoreData
        {
            public string Type = "OnServerInitialized";
            public string IP;
            public int TickRate;
            public int SaveInterval;
            public int WorldSize;
            public int Seed;
            public string Level;
            public string Hostname;
            public int QueryPort;
            public int Port;
            public string LevelURL;
            public int MaxPlayers;
            public string URL;
            public string HeaderImage;
            public string Description;
        }
        
        public void OnServerInitializedHandler()
        {
            OnServerInitializedData payload = new OnServerInitializedData
            {
                IP = RemoteRust.Call<string>("GetServerIP"),
                TickRate = Server.tickrate,
                SaveInterval = Server.saveinterval,
                WorldSize = Server.worldsize,
                Seed = Server.seed,
                Level = Server.level,
                Hostname = Server.hostname,
                QueryPort = Server.queryport,
                Port = Server.port,
                LevelURL = Server.levelurl,
                MaxPlayers = Server.maxplayers,
                URL = Server.url,
                HeaderImage = Server.headerimage,
                Description = Server.description
            };

            RemoteRust.Call("AddMessageToQueue", JsonConvert.SerializeObject(payload));
        }
        
        void OnServerInitialized(bool serverInitialized)
        {
            OnServerInitializedHandler();
        }
        
        #endregion
    }
}
